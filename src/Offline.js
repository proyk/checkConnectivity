import React from 'react';
import CountdownTimer from './CountdownTimer';

const Offline = ({ reconnect, startCountdownTimer }) => startCountdownTimer ?  (
  <div>
    Retrying in <CountdownTimer atTheEndOfTimer={reconnect} remainingTime={5} />
  </div>
): <noscript />;

export default Offline;

