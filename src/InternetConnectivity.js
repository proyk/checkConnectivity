import React, { Component } from 'react';
import { checkConnectivity } from './checkConnectivity.js';
import Retrying from './Retrying';
import Offline from './Offline';

class InternetConnectivity extends Component {
  constructor() {
    super();
    this.state = {
      online: true,
      reconnecting: false,
    };

    this.checkStatus = this.checkStatus.bind(this);
    this.reconnect = this.reconnect.bind(this);
    this.retrying = this.retrying.bind(this);
  }

  componentDidMount() {
    this.startInterval();
  }

  componentWillUnmount() {
    this.stopInterval();
  }

  startInterval() {
    this.interval = setInterval(this.checkStatus, 1000);
  }

  stopInterval() {
    clearInterval(this.interval);
  }

  checkStatus() {
    if (this.state.online) {
      this.setState({
        online: checkConnectivity(),
      });
    } else {
      this.stopInterval();
    }
  }

  reconnect() {
    this.setState({
      reconnecting: true,
    });
  }

  retrying() {
    setTimeout(() => {
      this.setState({
        online: checkConnectivity(),
        reconnecting: false,
      });
      this.startInterval();
    }, 2000);
  }

  render() {
    const { online, reconnecting } = this.state;

    return (
      <div>
        <Offline
          startCountdownTimer={!online && !reconnecting}
          reconnect={this.reconnect}
        />
        <Retrying show={!online && reconnecting} tryAgain={this.retrying} />
      </div>
    );
  }
};

export default InternetConnectivity;
