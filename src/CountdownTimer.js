import React, { Component } from 'react';

class CountdownTimer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      remainingTime: props.remainingTime,
    };

    this.tick = this.tick.bind(this);
  }

  componentDidMount() {
    this.startTimer();
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  startTimer() {
    this.interval = setInterval(this.tick, 1000);
  }

  stopTimer() {
    clearInterval(this.interval);
  }

  tick() {
    const { remainingTime } = this.state;
    const { atTheEndOfTimer } = this.props;

    if (remainingTime > 0) {
      this.setState({
        remainingTime: remainingTime - 1,
      });
    } else {
      atTheEndOfTimer();
      this.stopTimer();
    }

  }

  render() {
    return (
      <span>{this.state.remainingTime}</span>
    )
  }
}

CountdownTimer.propTypes = {
  remainingTime: React.PropTypes.number,
  atTheEndOfTimer: React.PropTypes.func,
};

CountdownTimer.defaultProps = {
  remainingTime: 0,
  atTheEndOfTimer: () => null,
};

export default CountdownTimer;
