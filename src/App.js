import React from 'react';
import logo from './logo.svg';
import './App.css';
import InternetConnectivity from './InternetConnectivity';

const App = () => (
  <div className="App">
    <div className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <InternetConnectivity />
    </div>
  </div>
);

export default App;
