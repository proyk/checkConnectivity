import React, { Component } from 'react';

class Retrying extends Component {
  componentWillReceiveProps(nextProps) {
    const { show, tryAgain } = nextProps;
    if (show) {
      tryAgain();
    }
  }

  render() {
    const { show } = this.props;
    return show ? <div>Retrying...</div> : <noscript />;
  }
}

export default Retrying;

